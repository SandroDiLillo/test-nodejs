import { connect } from 'mongoose' // import the function connect from from mongoose
import bodyParser from 'body-parser' // to handle HTTP request in express we need to install middleware which is body parser 
import express from 'express' //import express framework 
import UserModel from './UserModel' //import the model from the UserModel
import cors from 'cors' //  Cross Origin Resourse Sharing protection, to block browser request for unwanted origins

connect('mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority', { useNewUrlParser: true })
// connect('mongodb+srv://root:root@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority', { useNewUrlParser: true })

const app = express() // Creates an Express application. The express() function is a top-level function exported by the express module.
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true })) // set express middleware for bodyparse url read
app.use(bodyParser.json()) 

app.get('/', (req, res) => { //return on the / route the string "just a test"
  res.send('Just a test')
})


app.get('/users', (req, res) => { // on the /users route return a list of users ojects from the mongoose model 
  UserModel.find((err, results) => {
    if (err) res.send(err) //validation
    else res.send(results)
  })
})

app.post('/users', (req, res) => { // post route to save user inside the database 
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)// validation
    else res.send(newUser)
  })
})


// Find user and show the edit form
// 1. I create middleware to modify the user
// 2. i retrieve the user ID from the params
//  3. I use one of the static helper functions provided by mongoose "findById"
// 4. if the id does not exist in the database, I insert a redirect to the initial page
// 5. if it finds the id show the result inside the form

app.get('/update-user/:userId', (req, res, next) => {
  const userId = req.params.userId
  UserModel.findById(userId)
    .then(user => {
      if (!user) {
        return res.redirect('/');
      }
      res.send(result);// here we should have the editing view with the form 
    })
    .catch(err => console.log(err));
});

//update and save
// 1. define, in a constant, the new values
// 2. use the model created with Mongoose to create a new user
// 3. save the new user

app.post('/update-user', (req, res, next) => {
  const userId = req.body.userId;
  const updatedEmail = req.body.email;
  const updatedFirstName = req.body.firstName;
  const updatedLastName = req.body.lastName;
  const updatedPassword = req.body.password;

  const user = new UserMddel(
    updatedEmail,
    updatedFirstName,
    updatedLastName,
    updatedPassword,
    userId
  );

  user.save((err, updatedUser) => {
    if (err) res.send(err)
    else res.send(updatedUser)
  })
})

//delete user

// 1. i retrieve the user ID from the value input hidden (user._id)-> name(userId) (which will be sent when the delete button type submit is clicked)
// 2. I use one of the static helper functions provided by mongoose "findByIdAndRemove" to find and delete the user

app.post('/delete-user', (req, res, next) => {
  const userId = req.body.userId;
  UserModel.findByIdAndRemove(userId)
    .then(result => {
      console.log('DESTROYED USER');
      res.redirect('/users');
    })
    .catch(err => console.log(err));
});


// handle request error
app.use((err, req, res, next) => {
  res.status(err.status || 500).send(err.message || 'Internal Server Error')
});
app.use((err, req, res, next) => {
  res.status(404).send(err.message || 'Page not found');
});

app.listen(8080, () => console.log('Example app listening on port 8080!'))
