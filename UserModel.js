import { Schema, model } from 'mongoose' //import and defining our mongoose schema. Each schema maps to a MongoDB collection and defines the shape of the documents within that collection. 

const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true }, //define the validation roule for the data (type string, required ecc.). ex. user's email must be unique 
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

export default model('User', UserSchema)
